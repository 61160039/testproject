import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:todo_list/firebase_api.dart';
import 'package:todo_list/model/todo.dart';

class TodosProvider extends ChangeNotifier {
  List<Todo> _todos = [];

  List<Todo> get todos => _todos.where((todo) => todo.isDone == false).toList();
  List<Todo> get todosCompleted =>
      _todos.where((todo) => todo.isDone == true).toList();

  void addTodo(Todo todo) {
    FirebaseApi.createTodo(todo);
    //_todos.add(todo);
    notifyListeners();
  }

  void removeTodo(Todo todo) {
    //_todos.remove(todo);
    FirebaseApi.deleteTodo(todo);
    notifyListeners();
  }

  bool toggleTodoStatus(Todo todo) {
    todo.isDone = !todo.isDone;
    //print(todo.isDone);
    FirebaseApi.updateToggleStatus(todo);
    notifyListeners();
    return todo.isDone;
  }

  void updateTodo(Todo todo, String title, String description) {
    todo.title = title;
    todo.description = description;
    FirebaseApi.updateStatus(todo);
    notifyListeners();
  }
}
