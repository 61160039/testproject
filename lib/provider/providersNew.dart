import 'package:flutter/cupertino.dart';
import 'package:todo_list/firebase_api.dart';
import 'package:todo_list/model/todo.dart';

class TodosProvider {
  List<Todo> _todos = [];

  List<Todo> get todos => _todos.where((todo) => todo.isDone == false).toList();
  List<Todo> get todosCompleted =>
      _todos.where((todo) => todo.isDone == true).toList();

  void addTodo(Todo todo) {
    FirebaseApi.createTodo(todo);
    _todos.add(todo);
  }

  //void getTodo() {
  //_todos =  FirebaseApi.getTodo() as List<Todo>;
  //print(FirebaseApi.getTodo());
  //}

  void removeTodo(Todo todo) {
    _todos.remove(todo);
  }

  bool toggleTodoStatus(Todo todo) {
    todo.isDone = !todo.isDone;

    return todo.isDone;
  }

  void updateTodo(Todo todo, String title, String description) {
    todo.title = title;
    todo.description = description;
  }
}
