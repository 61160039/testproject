import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/model/todo.dart';
import 'package:todo_list/provider/providers.dart';
import 'package:todo_list/widget/todo_widget.dart';

class CompletedListWidget extends StatefulWidget {
  CompletedListWidget({Key? key}) : super(key: key);

  @override
  _CompletedListWidgetState createState() => _CompletedListWidgetState();
}

class _CompletedListWidgetState extends State<CompletedListWidget> {
  final Stream<QuerySnapshot> _todoStream = FirebaseFirestore.instance
      .collection('todo_list')
      .where('isDone', isEqualTo: true)
      //.orderBy('createdTime', descending: true)
      .snapshots();
  //final provider = Provider.of<TodosProvider>(context);
  List<Todo> todos = []; //provider.todosCompleted;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _todoStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading..');
          }
          todos = snapshot.data!.docs
              .map((DocumentSnapshot document) {
                Map<dynamic, dynamic> data =
                    document.data()! as Map<dynamic, dynamic>;
                //print(data['createdTime'].toDate().toString());
                //print(data['title']);
                var todo = new Todo(
                    createdTime: data['createdTime'].toDate(),
                    title: data['title'],
                    id: data['id'],
                    description: data['description'],
                    isDone: data['isDone']);
                return todo;
              })
              .cast<Todo>()
              .toList();

          return todos.isEmpty
              ? Center(
                  child: Text(
                    'No todos.',
                    style: TextStyle(fontSize: 20),
                  ),
                )
              : ListView.separated(
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.all(16),
                  separatorBuilder: (context, index) => Container(height: 8),
                  itemCount: todos.length,
                  itemBuilder: (context, index) {
                    final todo = todos[index];

                    return TodoWidget(todo: todo);
                  },
                );
        });
  }
}
