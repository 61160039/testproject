import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/model/todo.dart';
import 'package:todo_list/provider/providers.dart';
import 'package:todo_list/widget/task_form.dart';

class AddTaskWidget extends StatefulWidget {
  @override
  _AddTaskWidgetState createState() => _AddTaskWidgetState();
}

class _AddTaskWidgetState extends State<AddTaskWidget> {
  final _formKey = GlobalKey<FormState>();
  String title = '';
  String description = '';

  @override
  Widget build(BuildContext context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Add Task',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 8,
            ),
            TaskFormWidget(
              onChangedTitle: (title) =>
                  setState(() => this.title = title),
              onChangedDescription: (description) =>
                  setState(() => this.description = description),
              // onChangedDate: (selectedDate) =>
              //     setState(() => this.selectedDate = selectedDate),
              onSavedTask: addTodo,
            ),
          ],
        ),
      );

  void addTodo() {
    if (title.isNotEmpty) {
      var todo = new Todo(
        createdTime: DateTime.now(),
        title: title,
        id: DateTime.now().toString(),
        description: description,
      );

      // print(todo.title+" "+todo.description);
      final provider = Provider.of<TodosProvider>(context, listen: false);
      provider.addTodo(todo);

      Navigator.pop(context);
    }
  }
}
