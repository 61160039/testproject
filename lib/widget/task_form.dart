import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TaskFormWidget extends StatelessWidget {
  final String title;
  final String description;
  // DateTime date = DateTime.now();

  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;
  // final ValueChanged<DateTime> onChangedDate;
  final VoidCallback onSavedTask;

  TaskFormWidget({
    Key? key,
    this.title = '',
    this.description = '',
    required this.onChangedTitle,
    required this.onChangedDescription,
    // required this.onChangedDate,
    required this.onSavedTask,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            buildTitle(),
            SizedBox(
              height: 8,
            ),
            buildDescription(),
            SizedBox(
              height: 20,
            ),
            buildDateTime(context),
            SizedBox(
              height: 30,
            ),
            buildButton(),
          ],
        ),
      );

  Widget buildTitle() => TextFormField(
        maxLines: 1,
        initialValue: title,
        onChanged: onChangedTitle,
        // validator: (title) {
        //   if (title == null || title.isEmpty) {
        //     return 'The title cannot be empty';
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Title',
        ),
      );

  Widget buildDescription() => TextFormField(
        maxLines: 3,
        initialValue: description,
        onChanged: onChangedDescription,
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Description',
        ),
      );

  Widget buildButton() => SizedBox(
        width: double.infinity,
        child: ElevatedButton(onPressed: onSavedTask, child: Text('Save')),
      );

  Widget buildDateTime(BuildContext context) => TextFormField(
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          labelText: 'Due date',
        ),
      );
}
