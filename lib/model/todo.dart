import 'package:flutter/cupertino.dart';
import 'package:todo_list/snack_bar.dart';

class TodoField {
  static const createdTime = 'createdTime';
}
class Todo {
  DateTime createdTime;
  String title;
  String id;
  String description;
  bool isDone;

  Todo({
    required this.createdTime,
    required this.title,
    this.description = '',
    this.id = '',
    this.isDone = false
  });

  static Todo fromJson(Map<String, dynamic> json) => Todo(
        createdTime: DateTime.parse(json['createdTime']),
        title: json['title'],
        description: json['description'],
        id: json['id'],
        isDone: json['isDone'],
      );

  Map<String, dynamic> toJson() => {
    'createdTime': SnackBars.fromDateTimeToJson(createdTime),
    'title':title,
    'description': description,
    'id': id,
    'isDone': isDone,
  };
}